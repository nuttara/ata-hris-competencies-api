
SELECT t.id, 
	   t.name, 
       t.belongto_id, 
       b.name as belong_to
FROM `hris-matrix`.skill_groups t, `hris-matrix`.belongtos b 
WHERE b.id = t.belongto_id 
	and t.belongto_id = 2;
    
SELECT c.id, 
	   c.name, 
       b.name as belong_to,
       g.id as group_id 
FROM `hris-matrix`.skill_categories c, `hris-matrix`.skill_groups g, `hris-matrix`.belongtos b
WHERE c.group_id = g.id
	  and g.belongto_id = b.id
	  and b.id = 1
      and g.id = 1
ORDER BY id;
      
SELECT s.id, 
	   s.name, 
       b.name as belong_to,
       s.group_id,
       s.category_id
FROM `hris-matrix`.skills s
	, `hris-matrix`.skill_categories c
    , `hris-matrix`.skill_groups g
    , `hris-matrix`.belongtos b
WHERE g.belongto_id = b.id
      and s.group_id = g.id
      and s.category_id=c.id
      and c.group_id = g.id
      and s.group_id = 1
	  and s.category_id =1
	  and b.id = 1
ORDER BY id
;

SELECT s.id, 
	   s.name, 
       b.name as belong_to,
       s.group_id,
       s.category_id
FROM `hris-matrix`.skills s
    , `hris-matrix`.skill_groups g
    , `hris-matrix`.belongtos b
WHERE s.group_id = g.id
	  and g.belongto_id = b.id
      and s.group_id = 4
	  and b.id = 2
ORDER BY id
;

SELECT T.employee_id
	, T.emp_id
	, T.employee_skill_id
	, T.belongto_id
	, T.belongto_name
	, T.skill_group_id
	, T.skill_group_name
	, T.skill_category_id
	, T.skill_category_name
	, T.skill_id
	, T.skill_name
	, l.level as level
	, IFNULL(l.name,'') as level_name
FROM(
	SELECT es.employee_id
		, e.emp_id
		, es.id as employee_skill_id
		, es.belongto_id
		, b.name as belongto_name
		, es.skill_group_id
		, g.name skill_group_name
		, '' as skill_category_id
		, '' as skill_category_name
		, es.skill_id
		, s.name as skill_name
        , es.level as skill_level
	FROM `hris-matrix`.employee_skills es
		, `hris-matrix`.employees e
		, `hris-matrix`.belongtos b
		, `hris-matrix`.skill_groups g
		, `hris-matrix`.skills s
	WHERE es.employee_id = e.id
		and es.belongto_id = b.id
		and es.skill_group_id = g.id
		and es.skill_id = s.id
		and es.skill_category_id is null
		and e.emp_id =130
	UNION ALL
	SELECT es.employee_id
		, e.emp_id
		, es.id as employee_skill_id
		, es.belongto_id
		, b.name as belongto_name
		, es.skill_group_id
		, g.name skill_group_name
		, c.id as skill_category_id
		, c.name as skill_category_name
		, es.skill_id
		, s.name as skill_name
        , es.level as skill_level
	FROM `hris-matrix`.employee_skills es
		, `hris-matrix`.employees e
		, `hris-matrix`.belongtos b
		, `hris-matrix`.skill_groups g
		, `hris-matrix`.skill_categories c
		, `hris-matrix`.skills s
	WHERE es.employee_id = e.id
		and es.belongto_id = b.id
		and es.skill_group_id = g.id
		and es.skill_category_id = c.id
		and es.skill_id = s.id
		and es.belongto_id = 1
		and e.emp_id =130
) T LEFT JOIN `hris-matrix`.skill_levels l ON T.skill_level = l.id
ORDER BY belongto_id, skill_group_id, skill_category_id, skill_id
;
