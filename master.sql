SELECT * FROM `hris-matrix`.belongtos;
SELECT * FROM `hris-matrix`.departments;
SELECT * FROM `hris-matrix`.teams;
SELECT * FROM `hris-matrix`.titles;
SELECT * FROM `hris-matrix`.positions;

INSERT INTO `hris-matrix`.`belongtos`(`id`,`name`) VALUES(1,'ATA');
INSERT INTO `hris-matrix`.`belongtos`(`id`,`name`) VALUES(2,'BNC');

INSERT INTO `hris-matrix`.`departments`(`id`,`created_at`,`name_en`,`updated_at`) VALUES(1,now(),'IT',now());

INSERT INTO `hris-matrix`.`teams`(`id`,`created_at`,`name_en`,`updated_at`) VALUES(1,now(),'SOA',now());

INSERT INTO `hris-matrix`.`titles`(`id`,`created_at`,`name`,`updated_at`) VALUES(1,now(),'Mr.',now());

INSERT INTO `hris-matrix`.`positions`(`id`,`created_at`,`name_en`,`updated_at`) VALUES(1,now(),'Developer',now());

# Insert Skill Group
INSERT INTO `hris-matrix`.`skill_groups`(`id`,`created_at`,`name`,`updated_at`,`belongto_id`) VALUES (1, now(),'Technical skills',now(),1);
INSERT INTO `hris-matrix`.`skill_groups`(`id`,`created_at`,`name`,`updated_at`,`belongto_id`) VALUES (2, now(),'Functional skills',now(),1);
INSERT INTO `hris-matrix`.`skill_groups`(`id`,`created_at`,`name`,`updated_at`,`belongto_id`) VALUES (3, now(),'Languages',now(),1);
INSERT INTO `hris-matrix`.`skill_groups`(`id`,`created_at`,`name`,`updated_at`,`belongto_id`) VALUES (4, now(),'PRIMARY roles or skills',now(),2);
INSERT INTO `hris-matrix`.`skill_groups`(`id`,`created_at`,`name`,`updated_at`,`belongto_id`) VALUES (5, now(),'SECONDARY roles or skills',now(),2);
INSERT INTO `hris-matrix`.`skill_groups`(`id`,`created_at`,`name`,`updated_at`,`belongto_id`) VALUES (6, now(),'PRIMARY technological expertises',now(),2);
INSERT INTO `hris-matrix`.`skill_groups`(`id`,`created_at`,`name`,`updated_at`,`belongto_id`) VALUES (7, now(),'MINOR technological expertise',now(),2);
INSERT INTO `hris-matrix`.`skill_groups`(`id`,`created_at`,`name`,`updated_at`,`belongto_id`) VALUES (8, now(),'PRIMARY lines of business',now(),2);
INSERT INTO `hris-matrix`.`skill_groups`(`id`,`created_at`,`name`,`updated_at`,`belongto_id`) VALUES (9, now(),'SECONDARY lines of business',now(),2);

# Insert Skill Category
INSERT INTO `hris-matrix`.`skill_categories`(`id`,`created_at`,`name`,`updated_at`,`group_id`) VALUES(1,now(),'Application',now(),1);
INSERT INTO `hris-matrix`.`skill_categories`(`id`,`created_at`,`name`,`updated_at`,`group_id`) VALUES(2,now(),'Server technology',now(),1);
INSERT INTO `hris-matrix`.`skill_categories`(`id`,`created_at`,`name`,`updated_at`,`group_id`) VALUES(3,now(),'Development & scripting languages',now(),1);
INSERT INTO `hris-matrix`.`skill_categories`(`id`,`created_at`,`name`,`updated_at`,`group_id`) VALUES(4,now(),'Technical knowledge',now(),1);
INSERT INTO `hris-matrix`.`skill_categories`(`id`,`created_at`,`name`,`updated_at`,`group_id`) VALUES(5,now(),'Data management technology',now(),1);
INSERT INTO `hris-matrix`.`skill_categories`(`id`,`created_at`,`name`,`updated_at`,`group_id`) VALUES(6,now(),'Networking technology',now(),1);

# Insert Skill Level
INSERT INTO `hris-matrix`.`skill_levels`(`id`,`created_at`,`level`,`name`,`updated_at`) VALUES(1,now(),0,'Entry',now());
INSERT INTO `hris-matrix`.`skill_levels`(`id`,`created_at`,`level`,`name`,`updated_at`) VALUES(2,now(),1,'Intemediate',now());
INSERT INTO `hris-matrix`.`skill_levels`(`id`,`created_at`,`level`,`name`,`updated_at`) VALUES(3,now(),2,'Senior',now());
INSERT INTO `hris-matrix`.`skill_levels`(`id`,`created_at`,`level`,`name`,`updated_at`) VALUES(4,now(),3,'Undefined',now());

SELECT * FROM `hris-matrix`.titles;

SELECT * FROM `hris-matrix`.employees;

SELECT e.id as empid
	   ,t.name as title
	   , e.first_name
       , e.middle_name
       , e.last_name
FROM `hris-matrix`.employees e
	,`hris-matrix`.titles t
WHERE e.title_id = t.id
ORDER BY first_name, last_name, empid
;