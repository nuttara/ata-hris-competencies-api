SELECT TALL.employee_id
	, TALL.emp_id
    , t.name as title
    , TALL.first_name
    , IFNULL(TALL.middle_name, '') as middle_name
    , TALL.last_name
    , p.name_en as position
    , team.name_en as team
    , IFNULL(DATE_FORMAT(TALL.hire_date, "%Y-%m-%d"), '') as hire_date
    , TALL.manager_id as manager_id
    , mt.name as manager_title
    , em.first_name as manager_first_name
    , IFNULL(em.middle_name,'') as manager_middle_name
    , em.last_name as manager_last_name
	, TALL.employee_skill_id
	, TALL.belongto_id
	, TALL.belongto_name
	, TALL.skill_group_id
	, TALL.skill_group_name
	, TALL.skill_category_id
	, TALL.skill_category_name
	, TALL.skill_id
	, TALL.skill_name
	, l.level as level
	, IFNULL(l.name,'') as level_name
FROM(
	SELECT es.employee_id
		, e.emp_id
        , e.title_id
        , e.first_name
		, e.middle_name
		, e.last_name
        , e.hire_date
		, es.id as employee_skill_id
		, es.belongto_id
		, b.name as belongto_name
		, es.skill_group_id
		, g.name skill_group_name
		, '' as skill_category_id
		, '' as skill_category_name
		, es.skill_id
		, s.name as skill_name
        , es.level as skill_level
        , e.manager_id
	FROM `hris-matrix`.employee_skills es
		, `hris-matrix`.employees e
		, `hris-matrix`.belongtos b
		, `hris-matrix`.skill_groups g
		, `hris-matrix`.skills s
	WHERE es.employee_id = e.id
		and es.belongto_id = b.id
		and es.skill_group_id = g.id
		and es.skill_id = s.id
		and es.skill_category_id is null
		and e.emp_id =131
	UNION ALL
	SELECT es.employee_id
		, e.emp_id
        , e.title_id
        , e.first_name
		, e.middle_name
		, e.last_name
        , e.hire_date
		, es.id as employee_skill_id
		, es.belongto_id
		, b.name as belongto_name
		, es.skill_group_id
		, g.name skill_group_name
		, c.id as skill_category_id
		, c.name as skill_category_name
		, es.skill_id
		, s.name as skill_name
        , es.level as skill_level
        , e.manager_id
	FROM `hris-matrix`.employee_skills es
		, `hris-matrix`.employees e
		, `hris-matrix`.belongtos b
		, `hris-matrix`.skill_groups g
		, `hris-matrix`.skill_categories c
		, `hris-matrix`.skills s
	WHERE es.employee_id = e.id
		and es.belongto_id = b.id
		and es.skill_group_id = g.id
		and es.skill_category_id = c.id
		and es.skill_id = s.id
		and es.belongto_id = 1
		and e.emp_id =131
) TALL LEFT JOIN `hris-matrix`.skill_levels l ON TALL.skill_level = l.id
INNER JOIN `hris-matrix`.titles t ON TALL.title_id= t.id
INNER JOIN `hris-matrix`.positions p ON TALL.employee_id= p.id
INNER JOIN `hris-matrix`.teams team ON TALL.employee_id= team.id
LEFT JOIN `hris-matrix`.employees em ON TALL.manager_id = em.id
LEFT JOIN `hris-matrix`.titles mt ON em.title_id= mt.id
ORDER BY belongto_id, skill_group_id, skill_category_id, skill_id
;
