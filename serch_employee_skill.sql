SELECT e.id
	, e.emp_id as empid
    , t.name as title
    , e.first_name
    , e.middle_name
    , e.last_name
    , e.position_id
    , p.name_en as position_name
    , e.team_id
    , team.name_en as team_name
FROM `hris-matrix`.employees e
	,`hris-matrix`.titles t
    ,`hris-matrix`.positions p
    , `hris-matrix`.teams team
WHERE e.title_id = t.id
	and e.position_id = p.id
    and e.team_id = team.id
;