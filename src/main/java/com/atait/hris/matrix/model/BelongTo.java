package com.atait.hris.matrix.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NaturalId;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


@EntityListeners(AuditingEntityListener.class)
@Entity
@Table(name = "belongtos")
public class BelongTo {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @NaturalId
    @Column(length = 10)
    private BelongToName name;
    
    @OneToMany(
            mappedBy = "belongto",
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER,
            orphanRemoval = true
    )
    @Fetch(FetchMode.SELECT)
    @BatchSize(size = 100)
    private List<SkillGroup> skill_groups = new ArrayList<>();
    
    public BelongTo() {}
    
    public BelongTo(BelongToName name) {
    	this.name = name;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BelongToName getName() {
		return name;
	}

	public void setName(BelongToName name) {
		this.name = name;
	}

	public List<SkillGroup> getSkill_groups() {
		return skill_groups;
	}

	public void setSkill_groups(List<SkillGroup> skill_groups) {
		this.skill_groups = skill_groups;
	}

    

}
