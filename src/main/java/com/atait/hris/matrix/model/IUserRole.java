package com.atait.hris.matrix.model;

public interface IUserRole {
	
	Long getId();
	String getName();

}
