package com.atait.hris.matrix.model;


public enum  RoleName {
	ROLE_USER,
	ROLE_CCO,
	ROLE_HR,
	ROLE_SYSADMIN
}
