package com.atait.hris.matrix.payload;

import java.util.List;

public class EmployeeResponse {
	
	List<IEmployeeResponseItem> employees;

	public List<IEmployeeResponseItem> getEmployees() {
		return employees;
	}

	public void setEmployees(List<IEmployeeResponseItem> employees) {
		this.employees = employees;
	}

}
