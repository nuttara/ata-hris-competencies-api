package com.atait.hris.matrix.payload;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.atait.hris.matrix.payload.empoyeeskill.SkillEditingRequestItem;

public class EmployeeSkillEditingRequest {

	@NotNull
	Long empid;
	
	@NotEmpty
	@Valid
	List<SkillEditingRequestItem> skills;
	
	public Long getEmpid() {
		return empid;
	}
	public void setEmpid(Long empid) {
		this.empid = empid;
	}
	public List<SkillEditingRequestItem> getSkills() {
		return skills;
	}
	public void setSkills(List<SkillEditingRequestItem> skills) {
		this.skills = skills;
	}
	
	@Override
	public String toString() {
		return "EmployeeSkillEditingRequest Employee ID : " + this.empid;
	}
}
