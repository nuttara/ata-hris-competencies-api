package com.atait.hris.matrix.payload;

import java.util.List;
import com.atait.hris.matrix.payload.empoyeeskill.BelongToResponseItem;

public class EmployeeSkillResponse {

	Long id;
	Long empid;
	String title;
	String first_name;
	String middle_name;
	String last_name;
	String position;
	String team;
	String hire_date;
	Long manager_id;
	String manager_title;
	String manager_first_name;
	String manager_middle_name;
	String manager_last_name;

	List<BelongToResponseItem> belong_tos;
	
	public EmployeeSkillResponse() {}

	public EmployeeSkillResponse(Long id
								, Long empid
								, String title
								, String first_name
								, String middle_name
								, String last_name
								, String position
								, String team
								, String hire_date
								, Long manager_id
								, String manager_title
								, String manager_first_name
								, String manager_middle_name
								, String manager_last_name
								) {
		this.id = id;
		this.empid = empid;
		this.title = title;
		this.first_name = first_name;
		this.middle_name = middle_name;
		this.last_name = last_name;
		this.position = position;
		this.team = team;
		this.hire_date = hire_date ;
		this.manager_id = manager_id;
		this.manager_title = manager_title;
		this.manager_first_name = manager_first_name;
		this.manager_middle_name = manager_middle_name;
		this.manager_last_name = manager_last_name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getEmpid() {
		return empid;
	}

	public void setEmpid(Long empid) {
		this.empid = empid;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getMiddle_name() {
		return middle_name;
	}

	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getHire_date() {
		return hire_date;
	}

	public void setHire_date(String hire_date) {
		this.hire_date = hire_date;
	}

	public Long getManager_id() {
		return manager_id;
	}

	public void setManager_id(Long manager_id) {
		this.manager_id = manager_id;
	}

	public String getManager_title() {
		return manager_title;
	}

	public void setManager_title(String manager_title) {
		this.manager_title = manager_title;
	}

	public String getManager_first_name() {
		return manager_first_name;
	}

	public void setManager_first_name(String manager_first_name) {
		this.manager_first_name = manager_first_name;
	}

	public String getManager_middle_name() {
		return manager_middle_name;
	}

	public void setManager_middle_name(String manager_middle_name) {
		this.manager_middle_name = manager_middle_name;
	}

	public String getManager_last_name() {
		return manager_last_name;
	}

	public void setManager_last_name(String manager_last_name) {
		this.manager_last_name = manager_last_name;
	}

	public List<BelongToResponseItem> getBelong_tos() {
		return belong_tos;
	}

	public void setBelong_tos(List<BelongToResponseItem> belong_tos) {
		this.belong_tos = belong_tos;
	}

}
