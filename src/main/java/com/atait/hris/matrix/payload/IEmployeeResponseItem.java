package com.atait.hris.matrix.payload;

public interface IEmployeeResponseItem {
	
	Long getEmpId();
	String getTitle();
	String getFirst_name();
	String getMiddle_name();
	String getLast_name();

}
