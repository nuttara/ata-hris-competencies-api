package com.atait.hris.matrix.payload;

import com.atait.hris.matrix.model.BelongToName;
import com.atait.hris.matrix.model.SkillLevelName;

public interface IEmployeeSkillRecordItem {
	Long getEmployee_id();
	Long getEmp_id();
	String getTitle();
	String getFirst_name();
	String getMiddle_name();
	String getLast_name();
	String getPosition();
	String getTeam();
	String getHire_date();
	Long getManager_id();
	String getManager_title();
	String getManager_first_name();
	String getManager_middle_name();
	String getManager_last_name();
	Long getEmployee_skill_id();
	Long getBelongto_id();
	BelongToName getBelongto_name();
	Long getskill_group_id();
	String getSkill_group_name();
    Long getskill_category_id();
    String getSkill_category_name();
    Long getSkill_id();
    String getSkill_name();
    SkillLevelName getLevel();
    String getLevel_name();
}
