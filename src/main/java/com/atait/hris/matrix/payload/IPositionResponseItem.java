package com.atait.hris.matrix.payload;

public interface IPositionResponseItem {

	Long getId();
	String getName();
}
