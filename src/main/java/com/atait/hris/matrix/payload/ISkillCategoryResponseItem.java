package com.atait.hris.matrix.payload;

public interface ISkillCategoryResponseItem {

	Long getId();
	String getName();
	String getBelong_to();
	Long getGroup_id();
}
