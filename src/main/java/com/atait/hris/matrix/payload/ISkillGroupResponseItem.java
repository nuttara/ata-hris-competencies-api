package com.atait.hris.matrix.payload;

public interface ISkillGroupResponseItem {
    Long getId();
    String getName();
    String getBelong_to();
}

