package com.atait.hris.matrix.payload;

import com.atait.hris.matrix.model.SkillLevelName;

public interface ISkillLevelResponseItem {
	
	SkillLevelName getLevel();
	String getName();

}
