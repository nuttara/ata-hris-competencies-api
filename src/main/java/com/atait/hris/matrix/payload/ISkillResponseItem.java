package com.atait.hris.matrix.payload;

public interface ISkillResponseItem {

	Long getId();
	String getName();
	String getBelong_to();
	Long getGroup_id();
	Long getCategory_id();

}
