package com.atait.hris.matrix.payload;

public interface ITeamResponseItem {
	
	Long getId();
	String getName_en();
	String getName_th();

}
