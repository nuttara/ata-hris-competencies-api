package com.atait.hris.matrix.payload;


public class JwtAuthenticationResponse {
    private String accessToken;
    private String tokenType = "Bearer";
    private Long employeeId;

    public JwtAuthenticationResponse(String accessToken, Long employeeId) {
        this.accessToken = accessToken;
        this.employeeId = employeeId;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}
}
