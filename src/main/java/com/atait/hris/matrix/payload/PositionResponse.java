package com.atait.hris.matrix.payload;

import java.util.List;

public class PositionResponse {
	
	List<IPositionResponseItem> positions;

	public List<IPositionResponseItem> getPositions() {
		return positions;
	}

	public void setPositions(List<IPositionResponseItem> positions) {
		this.positions = positions;
	}
	
}
