package com.atait.hris.matrix.payload;

import java.util.List;
import javax.validation.Valid;
import com.atait.hris.matrix.payload.empoyeeskill.SearchSkillRequestItem;

public class SearchEmployeeRequest {
	
	Long empid;
	Long positionid;
	Long teamid;
	
	@Valid
	List<SearchSkillRequestItem> skills;
	
	public Long getEmpid() {
		return empid;
	}
	public void setEmpid(Long empid) {
		this.empid = empid;
	}
	public Long getPositionid() {
		return positionid;
	}
	public void setPositionid(Long positionid) {
		this.positionid = positionid;
	}
	public Long getTeamid() {
		return teamid;
	}
	public void setTeamid(Long teamid) {
		this.teamid = teamid;
	}
	public List<SearchSkillRequestItem> getSkills() {
		return skills;
	}
	public void setSkills(List<SearchSkillRequestItem> skills) {
		this.skills = skills;
	}
	
	@Override
	public String toString() {
		return "SearchEmployeeRequest Employee ID : " + this.empid + "\n"
				+ ", Team : " + this.positionid + "\n"
				+ ", Position : " + this.teamid;
	}

}
