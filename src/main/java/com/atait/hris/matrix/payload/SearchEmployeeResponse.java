package com.atait.hris.matrix.payload;

import java.io.Serializable;
import java.util.List;
import com.atait.hris.matrix.payload.empoyeeskill.SearchEmployeeResponseItem;
import lombok.Data;

@Data
public class SearchEmployeeResponse implements Serializable{

	private static final long serialVersionUID = 1L;

	List<SearchEmployeeResponseItem> employees;
	
	public List<SearchEmployeeResponseItem> getEmployees() {
		return employees;
	}
	public void setEmployees(List<SearchEmployeeResponseItem> employees) {
		this.employees = employees;
	}

	
}
