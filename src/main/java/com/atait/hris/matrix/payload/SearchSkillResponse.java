package com.atait.hris.matrix.payload;

import java.util.List;

public class SearchSkillResponse {
	
	List<SearchSkillResponseItem> skills;

	public List<SearchSkillResponseItem> getSkills() {
		return skills;
	}

	public void setSkills(List<SearchSkillResponseItem> skills) {
		this.skills = skills;
	}

}
