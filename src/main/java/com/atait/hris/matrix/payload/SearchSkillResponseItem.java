package com.atait.hris.matrix.payload;

import java.util.List;

public class SearchSkillResponseItem {
	
	Long id;
	String name;
	String belong_to;
	Long group_id;
	Long category_id;
	List<ISkillLevelResponseItem> levels;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBelong_to() {
		return belong_to;
	}

	public void setBelong_to(String belong_to) {
		this.belong_to = belong_to;
	}

	public Long getGroup_id() {
		return group_id;
	}

	public void setGroup_id(Long group_id) {
		this.group_id = group_id;
	}

	public Long getCategory_id() {
		return category_id;
	}

	public void setCategory_id(Long category_id) {
		this.category_id = category_id;
	}

	public List<ISkillLevelResponseItem> getLevels() {
		return levels;
	}

	public void setLevels(List<ISkillLevelResponseItem> levels) {
		this.levels = levels;
	}

}
