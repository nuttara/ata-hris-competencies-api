package com.atait.hris.matrix.payload;

import java.util.List;

public class SkillCategoryResponse {
	
	List<ISkillCategoryResponseItem> categories;

	public List<ISkillCategoryResponseItem> getCategories() {
		return categories;
	}

	public void setCategories(List<ISkillCategoryResponseItem> categories) {
		this.categories = categories;
	}

}
