package com.atait.hris.matrix.payload;

import java.util.List;

public class SkillGroupResponse {
	
	List<ISkillGroupResponseItem> groups;

	public List<ISkillGroupResponseItem> getGroups() {
		return groups;
	}

	public void setGroups(List<ISkillGroupResponseItem> groups) {
		this.groups = groups;
	}

}
