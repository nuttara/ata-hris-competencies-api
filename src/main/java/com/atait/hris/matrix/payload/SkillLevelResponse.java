package com.atait.hris.matrix.payload;

import java.util.List;

public class SkillLevelResponse {
	
	private List<ISkillLevelResponseItem> levels;

	public List<ISkillLevelResponseItem> getLevels() {
		return levels;
	}

	public void setLevels(List<ISkillLevelResponseItem> levels) {
		this.levels = levels;
	}
	
	
}
