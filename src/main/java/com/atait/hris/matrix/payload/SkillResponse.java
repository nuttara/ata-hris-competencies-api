package com.atait.hris.matrix.payload;

import java.util.List;

public class SkillResponse {
	
	private List<ISkillResponseItem> skills;

	public List<ISkillResponseItem> getSkills() {
		return skills;
	}

	public void setSkills(List<ISkillResponseItem> skills) {
		this.skills = skills;
	}


}
