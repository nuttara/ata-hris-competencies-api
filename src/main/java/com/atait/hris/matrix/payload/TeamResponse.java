package com.atait.hris.matrix.payload;

import java.util.List;

public class TeamResponse {

	List<ITeamResponseItem> teams;

	public List<ITeamResponseItem> getTeams() {
		return teams;
	}

	public void setTeams(List<ITeamResponseItem> teams) {
		this.teams = teams;
	}
		
}
