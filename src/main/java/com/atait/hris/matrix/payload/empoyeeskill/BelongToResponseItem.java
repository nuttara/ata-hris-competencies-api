package com.atait.hris.matrix.payload.empoyeeskill;

import java.util.List;

import com.atait.hris.matrix.model.BelongToName;

public class BelongToResponseItem{
	BelongToName name;
    List<GroupResponseItem> groups;
    
    public BelongToResponseItem() {}
    
    public BelongToResponseItem(BelongToName name) {
    	this.name = name;
    }
    
	public BelongToName getName() {
		return name;
	}
	public void setName(BelongToName name) {
		this.name = name;
	}
	public List<GroupResponseItem> getGroups() {
		return groups;
	}
	public void setGroups(List<GroupResponseItem> groups) {
		this.groups = groups;
	}
    
}
