package com.atait.hris.matrix.payload.empoyeeskill;

import java.util.List;

public class GroupResponseItem{
	Long id;
	String name;
	List<CategoryResponseItem> categories;
	List<SkillResponseItem> skills;
	
	public GroupResponseItem() {}
	
	public GroupResponseItem(Long id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<CategoryResponseItem> getCategories() {
		return categories;
	}
	public void setCategories(List<CategoryResponseItem> categories) {
		this.categories = categories;
	}
	public List<SkillResponseItem> getSkills() {
		return skills;
	}
	public void setSkills(List<SkillResponseItem> skills) {
		this.skills = skills;
	}
	
}
