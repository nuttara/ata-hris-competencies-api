package com.atait.hris.matrix.payload.empoyeeskill;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.atait.hris.matrix.model.SkillLevelName;

public class LevelEditingRequestItem {

	@NotNull
	SkillLevelName level;
	
	@NotBlank
	String name;

	public SkillLevelName getLevel() {
		return level;
	}

	public void setLevel(SkillLevelName level) {
		this.level = level;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
