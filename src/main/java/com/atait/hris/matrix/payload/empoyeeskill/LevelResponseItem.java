package com.atait.hris.matrix.payload.empoyeeskill;

import com.atait.hris.matrix.model.SkillLevelName;

public class LevelResponseItem{
	
	SkillLevelName level;
	String name;
	
	public LevelResponseItem() {}
	
	public LevelResponseItem(SkillLevelName level, String name) {
		this.level = level;
		this.name = name;
	}
	
	public SkillLevelName getLevel() {
		return level;
	}
	public void setLevel(SkillLevelName level) {
		this.level = level;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
