package com.atait.hris.matrix.payload.empoyeeskill;

public class SearchEmployeeResponseItem {
	Long empid;
    String title;
    String first_name;
    String middle_name;
    String last_name;
    Long position_id;
    String position_name;
    Long team_id;
    String team_name;
    
    public SearchEmployeeResponseItem() {}
    
    public SearchEmployeeResponseItem(
									Long empid,
								    String title,
								    String first_name,
								    String middle_name,
								    String last_name,
								    Long position_id,
								    String position_name,
								    Long team_id,
								    String team_name) {
    	this.empid = empid;
    	this.title = title;
    	this.first_name = first_name;
    	this.middle_name = middle_name;
    	this.last_name = last_name;
    	this.position_id = position_id;
    	this.position_name = position_name;
    	this.team_id = team_id;
    	this.team_name = team_name;
    	
    }
    
	public Long getEmpid() {
		return empid;
	}
	public void setEmpid(Long empid) {
		this.empid = empid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getMiddle_name() {
		return middle_name;
	}
	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public Long getPosition_id() {
		return position_id;
	}
	public void setPosition_id(Long position_id) {
		this.position_id = position_id;
	}
	public String getPosition_name() {
		return position_name;
	}
	public void setPosition_name(String position_name) {
		this.position_name = position_name;
	}
	public Long getTeam_id() {
		return team_id;
	}
	public void setTeam_id(Long team_id) {
		this.team_id = team_id;
	}
	public String getTeam_name() {
		return team_name;
	}
	public void setTeam_name(String team_name) {
		this.team_name = team_name;
	}
    
}
