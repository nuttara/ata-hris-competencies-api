package com.atait.hris.matrix.payload.empoyeeskill;

public class SearchSkillRequestItem extends SkillEditingRequestItem{

	@Override
	public String toString() {
		return "SearchSkillRequestItem Skill ID : " + this.getId() + "\n"
				+ ", Belong To : " + this.getBelong_to() + "\n"
				+ ", Group ID : " + this.getCategory_id() + "\n"
				+ ", Category : " + this.getGroup_id() + "\n"
				+ ", Kill Name : " + this.getName();
	}
}
