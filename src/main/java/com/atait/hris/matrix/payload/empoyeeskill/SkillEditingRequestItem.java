package com.atait.hris.matrix.payload.empoyeeskill;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import com.atait.hris.matrix.model.BelongToName;

public class SkillEditingRequestItem {
	
	@NotNull
	Long id;
	
	@NotNull
    BelongToName belong_to;
    
	@NotNull
	Long group_id;
    
	Long category_id;
	
    @NotBlank
	String name;
    
    @Valid
    List<LevelEditingRequestItem> levels;
    
    @Override
    public String toString(){
    	return "SkillEditingRequestItem ID : " + this.id + "\n" +
    			", Belongto Name : " + this.belong_to + "\n" +
    			", Group Id : " + this.group_id + "\n" +
    			", Category Id : " + this.category_id + "\n" +
    			", Name : " + this.name;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BelongToName getBelong_to() {
		return belong_to;
	}

	public void setBelong_to(BelongToName belong_to) {
		this.belong_to = belong_to;
	}

	public Long getGroup_id() {
		return group_id;
	}

	public void setGroup_id(Long group_id) {
		this.group_id = group_id;
	}

	public Long getCategory_id() {
		return category_id;
	}

	public void setCategory_id(Long category_id) {
		this.category_id = category_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<LevelEditingRequestItem> getLevels() {
		return levels;
	}

	public void setLevels(List<LevelEditingRequestItem> levels) {
		this.levels = levels;
	}

}
