package com.atait.hris.matrix.payload.empoyeeskill;

import java.util.List;

import com.atait.hris.matrix.model.BelongToName;

public class SkillResponseItem{
	Long id;
    BelongToName belong_to;
    Long group_id;
    Long category_id;
    String name;
    List<LevelResponseItem> levels;
    
    public SkillResponseItem() {}
    
    public SkillResponseItem(Long id, BelongToName belong_to, Long group_id,Long category_id, String name) {
    	this.id = id;
    	this.belong_to = belong_to;
    	this.group_id = group_id;
    	this.category_id = category_id;
    	this.name = name;
    }
    
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public BelongToName getBelong_to() {
		return belong_to;
	}
	public void setBelong_to(BelongToName belong_to) {
		this.belong_to = belong_to;
	}
	public Long getGroup_id() {
		return group_id;
	}
	public void setGroup_id(Long group_id) {
		this.group_id = group_id;
	}
	public Long getCategory_id() {
		return category_id;
	}
	public void setCategory_id(Long category_id) {
		this.category_id = category_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<LevelResponseItem> getLevels() {
		return levels;
	}
	public void setLevels(List<LevelResponseItem> levels) {
		this.levels = levels;
	}

}