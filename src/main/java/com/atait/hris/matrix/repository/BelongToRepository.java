package com.atait.hris.matrix.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.atait.hris.matrix.model.BelongTo;
import com.atait.hris.matrix.model.BelongToName;

@Repository
public interface BelongToRepository extends JpaRepository<BelongTo, Long> {
	
	BelongTo findByName(BelongToName belongToName);

}
