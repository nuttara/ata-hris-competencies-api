package com.atait.hris.matrix.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.atait.hris.matrix.model.Employee;
import com.atait.hris.matrix.payload.IEmployeeResponseItem;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
	
	Optional<Employee> findByEmail(String email);
	
    Optional<Employee> findByUsernameOrEmail(String username, String email);

    List<Employee> findByIdIn(List<Long> userIds);

    Optional<Employee> findByUsername(String username);
    
    Optional<Employee> findByEmpId(Long empId);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
    
    Boolean existsByEmpId(Long empId);
    
    String sqlAllEmployee = "SELECT e.emp_id as empid \n" + 
    		"	   ,t.name as title \n" + 
    		"	   , e.first_name \n" + 
    		"      , e.middle_name \n" + 
    		"      , e.last_name \n" + 
    		" FROM employees e \n" + 
    		"	   ,titles t \n" + 
    		" WHERE e.title_id = t.id \n" +
    		" ORDER BY first_name, last_name, empid";
	@Query(value = sqlAllEmployee, nativeQuery = true)
    List<IEmployeeResponseItem> getAllEmployeeForSearch();

}
