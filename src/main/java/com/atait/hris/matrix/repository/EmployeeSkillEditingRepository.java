package com.atait.hris.matrix.repository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.atait.hris.matrix.model.BelongTo;
import com.atait.hris.matrix.model.Employee;
import com.atait.hris.matrix.model.EmployeeSkill;
import com.atait.hris.matrix.model.Skill;
import com.atait.hris.matrix.model.SkillCategory;
import com.atait.hris.matrix.model.SkillGroup;
import com.atait.hris.matrix.model.SkillLevel;
import com.atait.hris.matrix.model.SkillLevelName;
import com.atait.hris.matrix.payload.EmployeeSkillEditingRequest;
import com.atait.hris.matrix.payload.empoyeeskill.LevelEditingRequestItem;
import com.atait.hris.matrix.payload.empoyeeskill.SkillEditingRequestItem;
import com.atait.hris.matrix.security.UserPrincipal;

@Repository
public class EmployeeSkillEditingRepository {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	BelongToRepository belongtoRepository;
	
	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	SkillGroupRepository skillGroupRepository;
	
	@Autowired
	SkillCategoryRepository skillCategoryRepository;
	
	@Autowired
	SkillRepository skillRepository;
	
	@Autowired
	SkillLevelRepository levelRepository;
	
	@Autowired
	EmployeeSkillRepository employeeSkillRepository;

	@Transactional
	public boolean updateEmployeeSkill(EmployeeSkillEditingRequest employeeSkill, UserPrincipal currentUser) {
		System.out.println(employeeSkill.toString());
		boolean updatedSucees = false;
		try {
			
			Employee emp = employeeRepository.findByEmpId(employeeSkill.getEmpid()).get();
			entityManager.createNativeQuery("DELETE FROM employee_skills WHERE employee_id = ?")
					.setParameter(1, emp.getId())
					.executeUpdate();
			for (SkillEditingRequestItem skill : employeeSkill.getSkills()) {
				List<LevelEditingRequestItem> listLevels = skill.getLevels();
				BelongTo belongto =null;
				SkillGroup skillGroup =null;
				SkillCategory skillCategory =null;
				Skill skillobj = null;
				SkillLevel level = null;
				belongto=belongtoRepository.findByName(skill.getBelong_to());
				skillGroup = skillGroupRepository.getOne(skill.getGroup_id());
				if(skill.getCategory_id() != null) {
					skillCategory = skillCategoryRepository.getOne(skill.getCategory_id());
				}
				skillobj = skillRepository.getOne(skill.getId());
				if (listLevels.size() > 0) {
					SkillLevelName skillLevel = (SkillLevelName) listLevels.get(0).getLevel();
					level = levelRepository.findByLevel(skillLevel);
				}
				EmployeeSkill empSkill = new EmployeeSkill();
				empSkill.setEmployee(emp);
				empSkill.setBelongto(belongto);
				empSkill.setSkill_group(skillGroup);
				empSkill.setSkill_category(skillCategory);
				empSkill.setSkill(skillobj);
				empSkill.setLevel(level);
				employeeSkillRepository.save(empSkill);
				/*
				String sql_insert = "INSERT INTO employee_skills(`created_at`,`updated_at`,`belongto_id`,`employee_id`,`skill_id`,`skill_category_id`,`skill_group_id`,`level`) \n"
						+ "VALUES(now()" + ",now()" + ",?" + ",?" + ",?" + ",?" + ",?" + ",?" + ");";
				entityManager.createNativeQuery(sql_insert)
						.setParameter(1, belongto.getId())
						.setParameter(2, employeeSkill.getEmpid())
						.setParameter(3, skill.getId())
						.setParameter(4, skill.getCategory_id() == null ? null : skill.getCategory_id())
						.setParameter(5, skill.getGroup_id())
						.setParameter(6, level == null ? null : level.getId()).executeUpdate();
				System.out.println(skill.toString());
				*/
			}
			updatedSucees = true;
		} catch (Exception e) {
			updatedSucees = false;
		}
		return updatedSucees;
	}

}
