package com.atait.hris.matrix.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.atait.hris.matrix.model.EmployeeSkill;
import com.atait.hris.matrix.payload.IEmployeeSkillRecordItem;

@Repository
public interface EmployeeSkillRepository extends JpaRepository<EmployeeSkill, Long>{
	
	String sql = "SELECT TALL.employee_id \n" + 
			"	 , TALL.emp_id \n" + 
			"    , t.name as title \n" + 
			"    , TALL.first_name \n" + 
			"    , IFNULL(TALL.middle_name, '') as middle_name \n" + 
			"    , TALL.last_name \n" + 
			"    , p.name_en as position \n" + 
			"    , team.name_en as team \n" + 
			"    , IFNULL(DATE_FORMAT(TALL.hire_date, \"%Y-%m-%d\"), '') as hire_date \n" + 
			"    , TALL.manager_id as manager_id \n" + 
			"    , mt.name as manager_title \n" + 
			"    , em.first_name as manager_first_name \n" + 
			"    , IFNULL(em.middle_name,'') as manager_middle_name \n" + 
			"    , em.last_name as manager_last_name \n" + 
			"	 , TALL.employee_skill_id \n" + 
			"	 , TALL.belongto_id \n" + 
			"	 , TALL.belongto_name \n" + 
			"	 , TALL.skill_group_id \n" + 
			"	 , TALL.skill_group_name \n" + 
			"	 , TALL.skill_category_id \n" + 
			"	 , TALL.skill_category_name \n" + 
			"	 , TALL.skill_id \n" + 
			"	 , TALL.skill_name \n" + 
			"	 , l.level as level \n" + 
			"	 , IFNULL(l.name,'') as level_name \n" + 
			"FROM( \n" + 
			"	SELECT es.employee_id \n" + 
			"		, e.emp_id \n" + 
			"       , e.title_id \n" + 
			"       , e.first_name \n" + 
			"		, e.middle_name \n" + 
			"		, e.last_name \n" + 
			"       , e.hire_date \n" + 
			"		, es.id as employee_skill_id \n" + 
			"		, es.belongto_id \n" + 
			"		, b.name as belongto_name \n" + 
			"		, es.skill_group_id \n" + 
			"		, g.name skill_group_name \n" + 
			"		, '' as skill_category_id \n" + 
			"		, '' as skill_category_name \n" + 
			"		, es.skill_id \n" + 
			"		, s.name as skill_name \n" + 
			"       , es.level as skill_level \n" + 
			"       , e.manager_id \n" + 
			"       , e.team_id \n" + 
			"       , e.position_id \n" +
			"	FROM employee_skills es \n" + 
			"		, employees e \n" + 
			"		, belongtos b \n" + 
			"		, skill_groups g \n" + 
			"		, skills s \n" + 
			"	WHERE es.employee_id = e.id \n" + 
			"		and es.belongto_id = b.id \n" + 
			"		and es.skill_group_id = g.id \n" + 
			"		and es.skill_id = s.id \n" + 
			"		and es.skill_category_id is null \n" + 
			"		and e.emp_id = :emp_id \n" + 
			"	UNION ALL \n" + 
			"	SELECT es.employee_id \n" + 
			"		, e.emp_id \n" + 
			"       , e.title_id \n" + 
			"       , e.first_name \n" + 
			"		, e.middle_name \n" + 
			"		, e.last_name \n" + 
			"       , e.hire_date \n" + 
			"		, es.id as employee_skill_id \n" + 
			"		, es.belongto_id \n" + 
			"		, b.name as belongto_name \n" + 
			"		, es.skill_group_id \n" + 
			"		, g.name skill_group_name \n" + 
			"		, c.id as skill_category_id \n" + 
			"		, c.name as skill_category_name \n" + 
			"		, es.skill_id \n" + 
			"		, s.name as skill_name \n" + 
			"       , es.level as skill_level \n" + 
			"       , e.manager_id \n" + 
			"       , e.team_id \n" + 
			"       , e.position_id \n" +
			"	FROM employee_skills es \n" + 
			"		, employees e \n" + 
			"		, belongtos b \n" + 
			"		, skill_groups g \n" + 
			"		, skill_categories c \n" + 
			"		, skills s \n" + 
			"	WHERE es.employee_id = e.id \n" + 
			"		and es.belongto_id = b.id \n" + 
			"		and es.skill_group_id = g.id \n" + 
			"		and es.skill_category_id = c.id \n" + 
			"		and es.skill_id = s.id \n" + 
			"		and es.belongto_id = 1 \n" + 
			"		and e.emp_id = :emp_id \n" + 
			") TALL LEFT JOIN skill_levels l ON TALL.skill_level = l.id \n" + 
			"INNER JOIN titles t ON TALL.title_id= t.id \n" + 
			"INNER JOIN positions p ON TALL.position_id= p.id \n" + 
			"INNER JOIN teams team ON TALL.team_id= team.id \n" + 
			"LEFT JOIN employees em ON TALL.manager_id = em.id \n" + 
			"LEFT JOIN titles mt ON em.title_id= mt.id \n" + 
			"ORDER BY belongto_id, skill_group_id, skill_category_id, skill_id";
	
	@Query(value = sql, nativeQuery = true)
	List<IEmployeeSkillRecordItem> getEmployeeSkills( @Param("emp_id") Long emp_id);

}
