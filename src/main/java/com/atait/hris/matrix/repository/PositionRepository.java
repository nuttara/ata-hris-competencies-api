package com.atait.hris.matrix.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.atait.hris.matrix.model.Position;
import com.atait.hris.matrix.payload.IPositionResponseItem;

@Repository
public interface PositionRepository extends JpaRepository<Position, Long>{
	
	String sql = "SELECT p.id \n" + 
    		"	   ,p.name_en as name \n" + 
    		" FROM positions p \n" + 
    		" ORDER BY id";
	@Query(value = sql, nativeQuery = true)
    List<IPositionResponseItem> getAllPositionForSearch();

}
