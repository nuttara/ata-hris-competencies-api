package com.atait.hris.matrix.repository;

import com.atait.hris.matrix.model.IUserRole;
import com.atait.hris.matrix.model.Role;
import com.atait.hris.matrix.model.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.ArrayList;
import java.util.Optional;


@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
    
    String sql="SELECT id, name FROM roles WHERE name=:roleName";
    @Query(value = sql, nativeQuery = true)
    ArrayList<IUserRole> findByNameAsString(@Param("roleName") String roleName);
}
