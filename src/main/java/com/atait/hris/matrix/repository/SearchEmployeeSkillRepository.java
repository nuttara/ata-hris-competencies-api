package com.atait.hris.matrix.repository;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.SqlResultSetMapping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.atait.hris.matrix.model.SkillLevel;
import com.atait.hris.matrix.payload.SearchEmployeeRequest;
import com.atait.hris.matrix.payload.empoyeeskill.LevelEditingRequestItem;
import com.atait.hris.matrix.payload.empoyeeskill.SearchEmployeeResponseItem;
import com.atait.hris.matrix.payload.empoyeeskill.SearchSkillRequestItem;

@Repository
public class SearchEmployeeSkillRepository {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	SkillLevelRepository skillLevelRepository;
	
	public List<SearchEmployeeResponseItem> search(){
		List<SearchEmployeeResponseItem> lstResult = new ArrayList<SearchEmployeeResponseItem>();
		String sql ="SELECT e.emp_id as empid \n" + 
				"    , t.name as title \n" + 
				"    , e.first_name \n" + 
				"    , e.middle_name \n" + 
				"    , e.last_name \n" + 
				"    , e.position_id \n" + 
				"    , p.name_en as position_name \n" + 
				"    , e.team_id \n" + 
				"    , team.name_en as team_name \n" + 
				"FROM employees e \n" + 
				"	, titles t \n" + 
				"   , positions p \n" + 
				"   , teams team \n" + 
				"WHERE e.title_id = t.id \n" + 
				"	and e.position_id = p.id \n" + 
				"    and e.team_id = team.id \n" + 
				"";
		@SuppressWarnings("unchecked")
		List<Object[]> results = this.entityManager.createNativeQuery(sql).getResultList();

		results.stream().forEach((record) -> {
				Long empid = ((BigInteger) record[0]).longValue();
			    String title = (String) record[1];
			    String first_name = (String) record[2];
			    String middle_name = (String) record[3];
			    String last_name = (String) record[4];
			    Long position_id = ((BigInteger) record[5]).longValue();
			    String position_name = (String) record[6];
			    Long team_id = ((BigInteger) record[7]).longValue();
			    String team_name = (String) record[8];
			    SearchEmployeeResponseItem employee = new SearchEmployeeResponseItem();
			    employee.setEmpid(empid);
			    employee.setTitle(title);
			    employee.setFirst_name(first_name);
			    employee.setMiddle_name(middle_name);
			    employee.setLast_name(last_name);
			    employee.setPosition_id(position_id);
			    employee.setPosition_name(position_name);
			    employee.setTeam_id(team_id);
			    employee.setTeam_name(team_name);
			    lstResult.add(employee);
		});
		
		return lstResult;
	}
	
	public List<SearchEmployeeResponseItem> searchEmployeeSkill(SearchEmployeeRequest seachRequestOject){
		@SqlResultSetMapping(name= "STATEMENT_SQLMAP", classes = {
		        @ConstructorResult(targetClass = SearchEmployeeResponseItem.class,
		            columns = {
		                @ColumnResult(name="empid",type = Long.class),
		                @ColumnResult(name="title",type = String.class),
		                @ColumnResult(name="first_name",type = String.class),
		                @ColumnResult(name="middle_name",type = String.class),
		                @ColumnResult(name="last_name",type = String.class),
		                @ColumnResult(name="position_id",type = Long.class),
		                @ColumnResult(name="position_name",type = String.class),
		                @ColumnResult(name="team_id",type = Long.class),
		                @ColumnResult(name="team_name",type = String.class)
		            }
		        )
		    }) @Entity class SQLMappingCfgEntity{@Id int id;}
		String sql ="SELECT distinct e.emp_id as empid \n" + 
				"    , t.name as title \n" + 
				"    , e.first_name \n" + 
				"    , e.middle_name \n" + 
				"    , e.last_name \n" + 
				"    , e.position_id \n" + 
				"    , p.name_en as position_name \n" + 
				"    , e.team_id \n" + 
				"    , team.name_en as team_name \n" + 
				"FROM employees e \n" + 
				"	, titles t \n" + 
				"   , positions p \n" + 
				"   , teams team \n" + 
				"   , employee_skills es \n" +
				"WHERE e.title_id = t.id \n" + 
				"	and e.position_id = p.id \n" + 
				"   and e.team_id = team.id \n" +
				"   and e.id = es.employee_id \n";
		if(seachRequestOject.getEmpid() != null) {
			sql = sql + " and e.emp_id = " + seachRequestOject.getEmpid() + " \n"; 
		}

		if(seachRequestOject.getTeamid() != null) {
			sql = sql + " and e.team_id = " + seachRequestOject.getTeamid() + " \n"; 
		}
		
		if(seachRequestOject.getPositionid() != null) {
			sql = sql + " and e.position_id = " + seachRequestOject.getPositionid() + " \n"; 
		}
		
		if (seachRequestOject.getSkills().size() > 0) {
			sql = sql + " and (" + " \n"; 
			int row = 1;
			for(SearchSkillRequestItem skill : seachRequestOject.getSkills()) {
				if(row > 1) {
					sql = sql + " or ";
				}
				sql = sql + " ( es.skill_id = " + skill.getId() + " and (@LEVELS_CRITERIA es.level is null) )" + " \n";
				String levelCriteria="";
				for(LevelEditingRequestItem level : skill.getLevels()) {
					SkillLevel skillLevel = skillLevelRepository.findByLevel(level.getLevel());
					levelCriteria  = levelCriteria + skillLevel.getId() + ",";
				}
				if(levelCriteria.length() > 0) {
					levelCriteria = levelCriteria.substring(0, levelCriteria.length()-1);
					levelCriteria = "es.level in (" + levelCriteria + ") or ";
					sql = sql.replace("@LEVELS_CRITERIA", levelCriteria);
				}
				else {
					sql = sql.replace("@LEVELS_CRITERIA", "");
				}
				row += 1;
			}
			sql = sql + " ) ";
		}
		
		@SuppressWarnings("unchecked")
		List<SearchEmployeeResponseItem> lstResult = this.entityManager.createNativeQuery(sql,"STATEMENT_SQLMAP").getResultList();
		return lstResult;
	}

}
