package com.atait.hris.matrix.repository;

import java.util.ArrayList;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.atait.hris.matrix.model.SkillCategory;
import com.atait.hris.matrix.payload.ISkillCategoryResponseItem;

@Repository
public interface SkillCategoryRepository extends JpaRepository<SkillCategory, Long> {
	
	String sql = "SELECT c.id, \n" + 
			"	    c.name, \n" + 
			"       b.name as belong_to,\n" + 
			"       g.id as group_id \n" + 
			" FROM skill_categories c, skill_groups g, belongtos b\n" + 
			" WHERE c.group_id = g.id\n" + 
			"	  and g.belongto_id = b.id\n" + 
			"	  and b.id = :belongto_id \n" + 
			"     and g.id = :group_id";

	@Query(value = sql, nativeQuery = true)
	ArrayList<ISkillCategoryResponseItem> findByBelongNametoAndGroupId(@Param("belongto_id") Long belongto_id, @Param("group_id") Long group_id);

}
