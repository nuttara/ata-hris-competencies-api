package com.atait.hris.matrix.repository;

import java.util.ArrayList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.atait.hris.matrix.model.SkillGroup;
import com.atait.hris.matrix.payload.ISkillGroupResponseItem;


@Repository
public interface SkillGroupRepository extends JpaRepository<SkillGroup, Long>{
	
	String sql ="SELECT t.id, \n" + 
			"	   t.name, \n" + 
			"      t.belongto_id, \n" + 
			"      b.name as belong_to\n" + 
			" FROM skill_groups t, belongtos b \n" + 
			" WHERE b.id = t.belongto_id \n" + 
			"	    and t.belongto_id = :belong_id";
	
	@Query(value = sql, nativeQuery = true)
	ArrayList<ISkillGroupResponseItem> findByBelongtoId(@Param("belong_id") Long belong_id);

}
