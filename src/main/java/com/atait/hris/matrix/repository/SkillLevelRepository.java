package com.atait.hris.matrix.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.atait.hris.matrix.model.SkillLevel;
import com.atait.hris.matrix.model.SkillLevelName;
import com.atait.hris.matrix.payload.ISkillLevelResponseItem;

@Repository
public interface SkillLevelRepository extends JpaRepository<SkillLevel, Long> {
	
	SkillLevel findByLevel(SkillLevelName level);
	 	
	@Query(value = "SELECT level, name FROM skill_levels", nativeQuery = true)
	List<ISkillLevelResponseItem> getAll();

}
