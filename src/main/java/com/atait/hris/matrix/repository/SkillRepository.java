package com.atait.hris.matrix.repository;

import java.util.ArrayList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.atait.hris.matrix.model.Skill;
import com.atait.hris.matrix.payload.ISkillResponseItem;


@Repository
public interface SkillRepository extends JpaRepository<Skill, Long>{

	String sqlAta = "SELECT s.id, \n" + 
			"	   s.name, \n" + 
			"      b.name as belong_to, \n" + 
			"      s.group_id, \n" + 
			"      s.category_id \n" + 
			" FROM skills s \n" + 
			"	   ,skill_categories c \n" + 
			"      ,skill_groups g \n" + 
			"      ,belongtos b \n" + 
			" WHERE s.category_id=c.id \n" + 
			"	  and s.group_id = g.id \n" + 
			"	  and c.group_id = g.id \n" + 
			"	  and g.belongto_id = b.id \n" + 
			"     and s.group_id = :group_id \n" + 
			"	  and s.category_id =:category_id \n" + 
			"	  and b.id = :belongto_id \n" + 
			" ORDER BY id";

	@Query(value = sqlAta, nativeQuery = true)
	ArrayList<ISkillResponseItem> findSkillWithCategory(  @Param("belongto_id") Long belongto_id
														  , @Param("group_id") Long group_id
														  , @Param("category_id") Long category_id
													);
   
	String sqlBnc = "SELECT s.id, \n" + 
			"	    s.name, \n" + 
			"       b.name as belong_to, \n" + 
			"       s.group_id, \n" + 
			"       s.category_id \n" + 
			" FROM skills s \n" + 
			"      ,skill_groups g \n" + 
			"      ,belongtos b \n" + 
			" WHERE s.group_id = g.id \n" + 
			"	   and g.belongto_id = b.id \n" + 
			"      and s.group_id = :group_id \n" + 
			"	   and b.id = :belongto_id \n" + 
			" ORDER BY id";
	@Query(value = sqlBnc, nativeQuery = true)
	ArrayList<ISkillResponseItem> findSkillWithoutCategory(  @Param("belongto_id") Long belongto_id
														  , @Param("group_id") Long group_id
													);
	
	String sqlSkill = "SELECT s.id, \n" + 
			"	   s.name, \n" + 
			"      b.name as belong_to, \n" + 
			"      s.group_id, \n" + 
			"      s.category_id \n" + 
			" FROM skills s \n" + 
			"      ,skill_groups g \n" + 
			"      ,belongtos b \n" + 
			" WHERE s.group_id = g.id \n" + 
			"	  and g.belongto_id = b.id \n" + 
			"	  and b.id = :belongto_id \n" + 
			" ORDER BY id";
	@Query(value = sqlSkill, nativeQuery = true)
	ArrayList<ISkillResponseItem> findSkillForSearch(@Param("belongto_id") Long belongto_id);
}
