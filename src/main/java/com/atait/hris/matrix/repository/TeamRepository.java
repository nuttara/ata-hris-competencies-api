package com.atait.hris.matrix.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.atait.hris.matrix.model.Team;
import com.atait.hris.matrix.payload.ITeamResponseItem;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long>{
	
	String sql = "SELECT t.id \n" + 
    		"	   ,t.name_en \n" + 
    		"	   ,t.name_th \n" + 
    		" FROM teams t \n" + 
    		" ORDER BY id";
	@Query(value = sql, nativeQuery = true)
    List<ITeamResponseItem> getAllTeamForSearch();

}
