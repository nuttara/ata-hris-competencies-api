package com.atait.hris.matrix.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.atait.hris.matrix.model.Title;

@Repository
public interface TitleRepository extends JpaRepository<Title, Long>{

}
