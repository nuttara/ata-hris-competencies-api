package com.atait.hris.matrix.restcontroller;

import com.atait.hris.matrix.payload.LoginRequest;
import com.atait.hris.matrix.payload.SignUpRequest;
import com.atait.hris.matrix.service.AuthenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/authen")
public class AuthController {
    
    @Autowired
    AuthenService authService;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
    	return authService.signin(loginRequest);
    }
    
	@PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
		
		return authService.signup(signUpRequest);
    }
}
