package com.atait.hris.matrix.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.atait.hris.matrix.security.CurrentUser;
import com.atait.hris.matrix.security.UserPrincipal;
import com.atait.hris.matrix.service.EmployeeSkillService;


@RestController
@RequestMapping("/api/v1/employee")
public class EmployeeController {
	
	@Autowired
    EmployeeSkillService employeeSkillService;
	
	@RequestMapping(value = "/profile/{empid}", method = RequestMethod.GET)
	public ResponseEntity<?> employee_profile(@CurrentUser UserPrincipal currentUser, @PathVariable Long empid) {
		return employeeSkillService.getEmployeeSkill(currentUser, empid);
	}

}
