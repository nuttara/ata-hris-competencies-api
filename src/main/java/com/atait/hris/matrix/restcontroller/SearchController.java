package com.atait.hris.matrix.restcontroller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.atait.hris.matrix.payload.SearchEmployeeRequest;
import com.atait.hris.matrix.service.SearchService;

@RestController
@RequestMapping("/api/v1/search")
public class SearchController {
	
	@Autowired
	SearchService searchService;
	
	@RequestMapping(value = "/employee/list", method = RequestMethod.GET)
	public ResponseEntity<?> getEmployee(){
		return searchService.getAllEmployee();
	}
	
	@RequestMapping(value = "/position/list", method = RequestMethod.GET)
	public ResponseEntity<?> getPosition(){
		return searchService.getAllPosition();
	}
	
	@RequestMapping(value = "/team/list", method = RequestMethod.GET)
	public ResponseEntity<?> getTeams(){
		return searchService.getAllTeam();
	}
	
	@RequestMapping(value = "/skill/list", method = RequestMethod.GET)
	public ResponseEntity<?> getAllSkills(){
		return searchService.getAllSkill();
	}
	
	@RequestMapping(value = "/employee/skill", method = RequestMethod.POST)
	public ResponseEntity<?> searchEmployeeSkill(@Valid @RequestBody SearchEmployeeRequest searchEmployee){
		return searchService.searchEmployee(searchEmployee);
	}

}
