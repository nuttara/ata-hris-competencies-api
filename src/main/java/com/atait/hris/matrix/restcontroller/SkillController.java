package com.atait.hris.matrix.restcontroller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.atait.hris.matrix.model.BelongToName;
import com.atait.hris.matrix.payload.EmployeeSkillEditingRequest;
import com.atait.hris.matrix.security.CurrentUser;
import com.atait.hris.matrix.security.UserPrincipal;
import com.atait.hris.matrix.service.SkillService;

@RestController
@RequestMapping("/api/v1/skill")
public class SkillController {

	@Autowired
	SkillService skillService;

	@RequestMapping(value = "/group/{belongto}", method = RequestMethod.GET)
	public ResponseEntity<?> getSkillGroup(@PathVariable BelongToName belongto){
		return skillService.getSkillGroup(belongto);
	}
	
	@RequestMapping(value = "/category/{belongto}/{group}", method = RequestMethod.GET)
	public ResponseEntity<?> skill_category(@PathVariable BelongToName belongto
											 ,@PathVariable Long group) {
		return skillService.getSkillCategory(belongto, group);
	}
	
	@RequestMapping(value = "/list/{belongto}/{group}/{category}", method = RequestMethod.GET)
	public ResponseEntity<?> skill(@PathVariable BelongToName belongto
											 , @PathVariable Long group
											 , @PathVariable Long category) {
		return skillService.getSkill(belongto, group, category);
	}
	
	@RequestMapping(value = "/level", method = RequestMethod.GET)
	public ResponseEntity<?> allLevels() {
		return skillService.getSkillLevel();
	}
	
	@RequestMapping(value = "/profile", method = RequestMethod.PUT)
	public ResponseEntity<?> updateProfile(@Valid @RequestBody EmployeeSkillEditingRequest employeeSkill, @CurrentUser UserPrincipal currentUser){
		return skillService.updateEmployeeeSkill(employeeSkill, currentUser);
	}
	
}
