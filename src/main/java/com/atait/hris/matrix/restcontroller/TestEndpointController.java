package com.atait.hris.matrix.restcontroller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
public class TestEndpointController {
	
	/*
	@RequestMapping(value = "/authen/signin", method = RequestMethod.POST)
	public @ResponseBody String signin() {
		return "End point : /api/v1/authen/signin , Method : POST";
	}
	
	@RequestMapping(value = "/authen/signup", method = RequestMethod.POST)
	public @ResponseBody String signup() {
		return "End point : /api/v1/authen/signup , Method : POST";
	}
	
	@RequestMapping(value = "/authen/signout", method = RequestMethod.POST)
	public @ResponseBody String authen() {
		return "End point : /api/v1/authen/signout , Method : POST";
	}
	
	@RequestMapping(value = "/employee/profile/{empid}", method = RequestMethod.GET)
	public @ResponseBody String employee_profile(@PathVariable String empid) {
		return "End point : /api/v1/employee/profile/{empid}=" + empid + " , Method : GET";
	}
	
	@RequestMapping(value = "/skill/group/{belongto}", method = RequestMethod.GET)
	public @ResponseBody String skill_group(@PathVariable String belongto) {
		return "End point : /api/v1/skill/group/{belongto}=" + belongto + " , Method : GET";
	}
	
	@RequestMapping(value = "/skill/category/{belongto}/{group}", method = RequestMethod.GET)
	public @ResponseBody String skill_category(@PathVariable String belongto
											 , @PathVariable String group) {
		return "End point : /api/v1/skill/category{belongto}=" + belongto + ", {group}=" + group + " , Method : GET";
	}
	
	@RequestMapping(value = "/skill/list/{belongto}/{group}/{category}", method = RequestMethod.GET)
	public @ResponseBody String skill_list(@PathVariable String belongto
										 , @PathVariable String group
										 , @PathVariable String category) {
		return "End point : /api/v1/skill/list/{belongto}=" + belongto + ", {group}=" + group + ", {category}=" + category + " , Method : GET";
	}
	
	@RequestMapping(value = "/skill/profile", method = RequestMethod.POST)
	public @ResponseBody String employee_profile() {
		return "End point : /api/v1/skill/profile, Method : POST";
	}
	
	@RequestMapping(value = "/search/employee/list", method = RequestMethod.GET)
	public @ResponseBody String search_employee_list() {
		return "End point : /api/v1/search/employee/list, Method : GET";
	}
	
	@RequestMapping(value = "/search/position/list", method = RequestMethod.GET)
	public @ResponseBody String search_position_list() {
		return "End point : /api/v1/search/position/list, Method : GET";
	}
	
	@RequestMapping(value = "/search/team/list", method = RequestMethod.GET)
	public @ResponseBody String search_team_list() {
		return "End point : /api/v1/search/team/list, Method : GET";
	}
	
	@RequestMapping(value = "/search/skill/list", method = RequestMethod.GET)
	public @ResponseBody String search_skill_list() {
		return "End point : /api/v1/search/skill/list, Method : GET";
	}
	
	@RequestMapping(value = "/search/employee/skill", method = RequestMethod.POST)
	public @ResponseBody String search_employee() {
		return "End point : /api/v1/search/employee/skill, Method : POST";
	}
	*/

}
