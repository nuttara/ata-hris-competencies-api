package com.atait.hris.matrix.service;

import java.util.Collections;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.atait.hris.matrix.exception.AppException;
import com.atait.hris.matrix.model.Department;
import com.atait.hris.matrix.model.Employee;
import com.atait.hris.matrix.model.IUserRole;
import com.atait.hris.matrix.model.Position;
import com.atait.hris.matrix.model.Role;
import com.atait.hris.matrix.model.Team;
import com.atait.hris.matrix.model.Title;
import com.atait.hris.matrix.payload.ApiResponse;
import com.atait.hris.matrix.payload.JwtAuthenticationResponse;
import com.atait.hris.matrix.payload.LoginRequest;
import com.atait.hris.matrix.payload.SignUpRequest;
import com.atait.hris.matrix.repository.DepartmentRepository;
import com.atait.hris.matrix.repository.EmployeeRepository;
import com.atait.hris.matrix.repository.PositionRepository;
import com.atait.hris.matrix.repository.RoleRepository;
import com.atait.hris.matrix.repository.TeamRepository;
import com.atait.hris.matrix.repository.TitleRepository;
import com.atait.hris.matrix.security.JwtTokenProvider;
import com.atait.hris.matrix.security.UserPrincipal;

@Service
public class AuthenService {
	
	@Autowired
    AuthenticationManager authenticationManager;
    
    @Autowired
    EmployeeRepository employeeRepository;
    
    @Autowired
    TitleRepository titleRepository;
    
    @Autowired
    DepartmentRepository departmentRepository;
    
    @Autowired
    TeamRepository teamRepository;
    
    @Autowired
    PositionRepository positionRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;
    
    public ResponseEntity<?> signin(LoginRequest loginRequest) {
    	Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);
        
        UserPrincipal userPrincipal = (UserPrincipal)authentication.getPrincipal();

        String jwt = tokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, userPrincipal.getEmployeeId()));
    }
    
    @SuppressWarnings("unchecked")
	public ResponseEntity<?> signup(SignUpRequest signUpRequest) {
    	if(employeeRepository.existsByUsername(signUpRequest.getUsername())) {
            return new ResponseEntity(new ApiResponse(false, "Username is already taken!"),
                    HttpStatus.BAD_REQUEST);
        }

        if(employeeRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"),
                    HttpStatus.BAD_REQUEST);
        }
        
        if(employeeRepository.existsByEmpId(signUpRequest.getEmpId())) {
            return new ResponseEntity(new ApiResponse(false, "Employee Id already in use!"),
                    HttpStatus.BAD_REQUEST);
        }

        Title title = titleRepository.findById(signUpRequest.getTitle_id())
        		.orElseThrow(() -> new AppException("Title are not set."));
        
        Department dept = departmentRepository.findById(signUpRequest.getDept_id())
        		.orElseThrow(() -> new AppException("Department are not set."));
        
        Team team = teamRepository.findById(signUpRequest.getTeam_id())
        		.orElseThrow(() -> new AppException("Teams are not set."));
        
        Position position = positionRepository.findById(signUpRequest.getPosition_id())
        		.orElseThrow(() -> new AppException("Position are not set."));
        
        Employee employee = new Employee();
        	employee.setEmpId(signUpRequest.getEmpId());
        	employee.setTitle(title);
        	employee.setDeparment(dept);
        	employee.setTeam(team);
        	employee.setPosition(position);
        	employee.setFirstName(signUpRequest.getFirstName());
        	employee.setMiddleName(signUpRequest.getMiddleName());
        	employee.setLastName(signUpRequest.getLastName());
        	employee.setManagerId(signUpRequest.getManagerId());
        	employee.setHireDate(signUpRequest.getHireDate());
        	employee.setBirthDate(signUpRequest.getBirthDate());
        	employee.setEmail(signUpRequest.getEmail());
        	employee.setUsername(signUpRequest.getUsername());
        	employee.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));
        	
        List<IUserRole> listRole= roleRepository.findByNameAsString(signUpRequest.getUserRole());
        if(listRole.size() < 1) {
        	throw(new AppException("Invalid Role Name."));
        }
//        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
//                .orElseThrow(() -> new AppException("User Role not set."));
        Role userRole = roleRepository.findById(listRole.get(0).getId())
                .orElseThrow(() -> new AppException("User Role not set."));

        employee.setRoles(Collections.singleton(userRole));

        Employee result = employeeRepository.save(employee);

        return ResponseEntity.ok(new ApiResponse(true, "Employee registered successfully"));
    }

}
