package com.atait.hris.matrix.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.atait.hris.matrix.exception.ResourceNotFoundException;
import com.atait.hris.matrix.model.BelongToName;
import com.atait.hris.matrix.payload.EmployeeSkillResponse;
import com.atait.hris.matrix.payload.IEmployeeSkillRecordItem;
import com.atait.hris.matrix.payload.empoyeeskill.BelongToResponseItem;
import com.atait.hris.matrix.payload.empoyeeskill.CategoryResponseItem;
import com.atait.hris.matrix.payload.empoyeeskill.GroupResponseItem;
import com.atait.hris.matrix.payload.empoyeeskill.LevelResponseItem;
import com.atait.hris.matrix.payload.empoyeeskill.SkillResponseItem;
import com.atait.hris.matrix.repository.EmployeeSkillRepository;
import com.atait.hris.matrix.security.UserPrincipal;

@Service
public class EmployeeSkillService {
	
	@Value("${app.group.has.category}")
	int APP_GROUP_HAS_CATEGORY;
	
	@Autowired
	EmployeeSkillRepository empSkillRepository;
	
	public ResponseEntity<?> getEmployeeSkill(UserPrincipal currentUser, Long empid){
		EmployeeSkillResponse empSkillRes = null;
		List<IEmployeeSkillRecordItem> listSkillItem = empSkillRepository.getEmployeeSkills(empid);
		
		int i = 1;
		for(IEmployeeSkillRecordItem record : listSkillItem) {
			if(i == 1) {
				empSkillRes = new EmployeeSkillResponse(0L
														, empid
														, record.getTitle()
														, record.getFirst_name()
														, record.getMiddle_name()
														, record.getLast_name()
														, record.getPosition()
														, record.getTeam()
														, record.getHire_date()
														, record.getManager_id()
														, record.getManager_title()
														, record.getManager_first_name()
														, record.getManager_middle_name()
														, record.getManager_last_name()
														);
				List<BelongToResponseItem> lstBelongTo = new ArrayList<BelongToResponseItem>();
				empSkillRes.setBelong_tos(lstBelongTo);
				List<GroupResponseItem> lstGroup = new ArrayList<GroupResponseItem>();
				List<CategoryResponseItem> lstCategory = new ArrayList<CategoryResponseItem>();
				List<SkillResponseItem> lstSkill = new ArrayList<SkillResponseItem>();
				BelongToResponseItem belongto = new BelongToResponseItem(record.getBelongto_name());
				belongto.setGroups(lstGroup);
				GroupResponseItem group = new GroupResponseItem(record.getskill_group_id(), record.getSkill_group_name());
				belongto.getGroups().add(group);
				group.setCategories(lstCategory);
				group.setSkills(lstSkill);				
				SkillResponseItem skill  = new SkillResponseItem(record.getSkill_id(), record.getBelongto_name(), record.getskill_group_id(), record.getskill_category_id(), record.getSkill_name());
				List<LevelResponseItem> levels = new ArrayList<LevelResponseItem>();
				skill.setLevels(levels);
				if(!record.getLevel_name().equals("")) {
					LevelResponseItem level_item = new LevelResponseItem(record.getLevel(), record.getLevel_name());
					skill.getLevels().add(level_item);
				}
				if(record.getSkill_category_name() != null) {
					CategoryResponseItem category = new CategoryResponseItem(record.getskill_category_id(), record.getSkill_category_name());
					List<SkillResponseItem> lstSkillUnderCategory = new ArrayList<SkillResponseItem>();
					category.setSkills(lstSkillUnderCategory);
					lstSkillUnderCategory.add(skill);
					group.getCategories().add(category);					
				}
				else {
					group.getSkills().add(skill);
				}
				empSkillRes.getBelong_tos().add(belongto);
			}
			else {
				
				BelongToResponseItem current_belongto = null;
				boolean existingBelongTo = false;
				for(BelongToResponseItem belongto_item : empSkillRes.getBelong_tos()) {
					if(belongto_item.getName().equals(record.getBelongto_name())) {
						existingBelongTo = true;
						current_belongto = belongto_item;
						break;
					}
				}
				if(! existingBelongTo)
				{
					current_belongto = new BelongToResponseItem(record.getBelongto_name());
					List<GroupResponseItem> lstGroupForNewBelongto = new ArrayList<GroupResponseItem>();
					current_belongto.setGroups(lstGroupForNewBelongto);
					empSkillRes.getBelong_tos().add(current_belongto);
				}
				
				GroupResponseItem current_group = null;
				boolean existingGroup = false;
				for(GroupResponseItem group_item: current_belongto.getGroups()) {
					if(group_item.getId().equals(record.getskill_group_id())) {
						existingGroup = true;
						current_group = group_item;
						break;
					}
				}
				if(! existingGroup) {
					current_group= new GroupResponseItem(record.getskill_group_id(), record.getSkill_group_name());
					List<CategoryResponseItem> lstCategoryForNewGroup = new ArrayList<CategoryResponseItem>();
					List<SkillResponseItem> lstSkillForNewGroup = new ArrayList<SkillResponseItem>();
					current_group.setCategories(lstCategoryForNewGroup);
					current_group.setSkills(lstSkillForNewGroup);
					current_belongto.getGroups().add(current_group);
				}
				
				SkillResponseItem skill  = new SkillResponseItem(record.getSkill_id(), record.getBelongto_name(), record.getskill_group_id(), record.getskill_category_id(), record.getSkill_name());
				List<LevelResponseItem> levels = new ArrayList<LevelResponseItem>();
				skill.setLevels(levels);
				if(!record.getLevel_name().equals("")) {
					LevelResponseItem level_item = new LevelResponseItem(record.getLevel(), record.getLevel_name());
					skill.getLevels().add(level_item);
				}
				if(record.getSkill_category_name() != null) {
					CategoryResponseItem current_category = null;
					boolean existingCategory = false;
					for(CategoryResponseItem category_item : current_group.getCategories()) {
						if(category_item.getId().equals(record.getskill_category_id())) {
							existingCategory = true;
							current_category = category_item;
							break;
						}
					}
					if(! existingCategory) {
						if(record.getBelongto_name().equals(BelongToName.ATA) && (record.getskill_group_id() <= APP_GROUP_HAS_CATEGORY)) {
							current_category = new CategoryResponseItem(record.getskill_category_id(), record.getSkill_category_name());
							List<SkillResponseItem> listSkillForNewCategory = new ArrayList<SkillResponseItem>();
							current_category.setSkills(listSkillForNewCategory);
							current_group.getCategories().add(current_category);
							current_category.getSkills().add(skill);
						}
						else {
							current_group.getSkills().add(skill);
						}
					}
					else {
						current_category.getSkills().add(skill);
					}
					
				}
				else {
					current_group.getSkills().add(skill);
				}
			}
			i += 1;
		}
		if(empSkillRes == null) {
			//empSkillRes = new EmployeeSkillResponse();
			throw new ResourceNotFoundException("Employee's skill", "Employee Id", empid);
		}
		return ResponseEntity.ok(empSkillRes);
	}
}
