package com.atait.hris.matrix.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.atait.hris.matrix.payload.EmployeeResponse;
import com.atait.hris.matrix.payload.IEmployeeResponseItem;
import com.atait.hris.matrix.payload.IPositionResponseItem;
import com.atait.hris.matrix.payload.ISkillLevelResponseItem;
import com.atait.hris.matrix.payload.ISkillResponseItem;
import com.atait.hris.matrix.payload.ITeamResponseItem;
import com.atait.hris.matrix.payload.PositionResponse;
import com.atait.hris.matrix.payload.SearchEmployeeRequest;
import com.atait.hris.matrix.payload.SearchEmployeeResponse;
import com.atait.hris.matrix.payload.SearchSkillResponse;
import com.atait.hris.matrix.payload.SearchSkillResponseItem;
import com.atait.hris.matrix.payload.TeamResponse;
import com.atait.hris.matrix.payload.empoyeeskill.SearchEmployeeResponseItem;
import com.atait.hris.matrix.repository.EmployeeRepository;
import com.atait.hris.matrix.repository.PositionRepository;
import com.atait.hris.matrix.repository.SearchEmployeeSkillRepository;
import com.atait.hris.matrix.repository.SkillLevelRepository;
import com.atait.hris.matrix.repository.SkillRepository;
import com.atait.hris.matrix.repository.TeamRepository;

@Service
public class SearchService {

	@Autowired
	EmployeeRepository employeeRepository;
	
	@Autowired
	PositionRepository positionRepository;
	
	@Autowired
	TeamRepository teamRepository;
	
	@Autowired
	SkillRepository skillRepository;
	
	@Autowired
	SkillLevelRepository skillLevelRepository;
	
	@Autowired
	SearchEmployeeSkillRepository searchEmployeeSkillRepository;
	
	public ResponseEntity<?> getAllEmployee(){
		EmployeeResponse employees = new EmployeeResponse();
		List<IEmployeeResponseItem> lstEmployee = employeeRepository.getAllEmployeeForSearch();
		employees.setEmployees(lstEmployee);
		return ResponseEntity.ok(employees);
	}
	
	public ResponseEntity<?> getAllPosition(){
		PositionResponse positions = new PositionResponse();
		List<IPositionResponseItem> lstPosition = positionRepository.getAllPositionForSearch();
		positions.setPositions(lstPosition);
		return ResponseEntity.ok(positions);
	}
	
	public ResponseEntity<?> getAllTeam(){
		TeamResponse teams = new TeamResponse();
		List<ITeamResponseItem> lstTeams = teamRepository.getAllTeamForSearch();
		teams.setTeams(lstTeams);
		return ResponseEntity.ok(teams);
	}
	
	public ResponseEntity<?> getAllSkill(){
		SearchSkillResponse searchSkillResponse = new SearchSkillResponse();
		List<SearchSkillResponseItem> listSearchSkillResponse = new ArrayList<SearchSkillResponseItem>();	
		List<ISkillResponseItem> listSkillAta = skillRepository.findSkillForSearch(1L);
		List<ISkillResponseItem> listSkillBnc = skillRepository.findSkillForSearch(2L);
		List<ISkillLevelResponseItem> listSkillLevel = skillLevelRepository.getAll();
		List<ISkillLevelResponseItem> emptyListSkillLevel = new ArrayList<ISkillLevelResponseItem>();
		
		for(ISkillResponseItem item : listSkillAta) {
			SearchSkillResponseItem skillResItem = new SearchSkillResponseItem();
			skillResItem.setId(item.getId());
			skillResItem.setBelong_to(item.getBelong_to());
			skillResItem.setGroup_id(item.getGroup_id());
			skillResItem.setCategory_id(item.getCategory_id());
			skillResItem.setName(item.getName());
			skillResItem.setLevels(listSkillLevel);
			listSearchSkillResponse.add(skillResItem);
		}
		
		for(ISkillResponseItem item : listSkillBnc) {
			SearchSkillResponseItem skillResItem = new SearchSkillResponseItem();
			skillResItem.setId(item.getId());
			skillResItem.setBelong_to(item.getBelong_to());
			skillResItem.setGroup_id(item.getGroup_id());
			skillResItem.setCategory_id(item.getCategory_id());
			skillResItem.setName(item.getName());
			skillResItem.setLevels(emptyListSkillLevel);
			listSearchSkillResponse.add(skillResItem);
		}
		searchSkillResponse.setSkills(listSearchSkillResponse);
		return ResponseEntity.ok(searchSkillResponse);
	}
	
	public ResponseEntity<?> searchEmployee(SearchEmployeeRequest seachRequestOject){
		SearchEmployeeResponse employeeResponse = new SearchEmployeeResponse();
		List<SearchEmployeeResponseItem> listEmployee = searchEmployeeSkillRepository.searchEmployeeSkill(seachRequestOject);
		employeeResponse.setEmployees(listEmployee);
		return ResponseEntity.ok(employeeResponse);
	}
	
}
