package com.atait.hris.matrix.service;

import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.atait.hris.matrix.model.BelongTo;
import com.atait.hris.matrix.model.BelongToName;
import com.atait.hris.matrix.payload.ApiResponse;
import com.atait.hris.matrix.payload.EmployeeSkillEditingRequest;
import com.atait.hris.matrix.payload.ISkillCategoryResponseItem;
import com.atait.hris.matrix.payload.ISkillGroupResponseItem;
import com.atait.hris.matrix.payload.ISkillLevelResponseItem;
import com.atait.hris.matrix.payload.ISkillResponseItem;
import com.atait.hris.matrix.payload.SkillCategoryResponse;
import com.atait.hris.matrix.payload.SkillGroupResponse;
import com.atait.hris.matrix.payload.SkillLevelResponse;
import com.atait.hris.matrix.payload.SkillResponse;
import com.atait.hris.matrix.repository.BelongToRepository;
import com.atait.hris.matrix.repository.EmployeeSkillEditingRepository;
import com.atait.hris.matrix.repository.SkillCategoryRepository;
import com.atait.hris.matrix.repository.SkillGroupRepository;
import com.atait.hris.matrix.repository.SkillLevelRepository;
import com.atait.hris.matrix.repository.SkillRepository;
import com.atait.hris.matrix.security.UserPrincipal;

@Service
public class SkillService {
	
	@Value("${app.group.has.category}")
	int APP_GROUP_HAS_CATEGORY;
	
	@Autowired
	SkillGroupRepository skillGroupRepo;
	
	@Autowired
	SkillCategoryRepository skillCategoryRepo;
	
	@Autowired
	SkillRepository skillRepo;
	
	@Autowired
	SkillLevelRepository skillLevelRepo;
	
	@Autowired
	BelongToRepository belongToRepo;
	
	@Autowired
	EmployeeSkillEditingRepository employeeSkillEditingRepository;
	
	public ResponseEntity<?> getSkillGroup(BelongToName belongtoName){
		SkillGroupResponse skillGroupRes = new SkillGroupResponse();
		BelongTo belongto = belongToRepo.findByName(belongtoName);
		List<ISkillGroupResponseItem> lstSkillGroup= skillGroupRepo.findByBelongtoId(belongto.getId());
		skillGroupRes.setGroups(lstSkillGroup);
		return ResponseEntity.ok(skillGroupRes);
	}
	
	public ResponseEntity<?> getSkillCategory(BelongToName belongtoName, Long group_id){
		SkillCategoryResponse skillCategoryRes = new SkillCategoryResponse();
		BelongTo belongto = belongToRepo.findByName(belongtoName);
		List<ISkillCategoryResponseItem> lstSkillCategory= skillCategoryRepo.findByBelongNametoAndGroupId(belongto.getId(), group_id);
		skillCategoryRes.setCategories(lstSkillCategory);
		return ResponseEntity.ok(skillCategoryRes);
	}
	
	public ResponseEntity<?> getSkill(BelongToName belongtoName, Long group_id, Long category_id){
		SkillResponse skillsRes = new SkillResponse();
		BelongTo belongto = belongToRepo.findByName(belongtoName);
		List<ISkillResponseItem> lstSkills;
		
		if(belongtoName.equals(BelongToName.ATA)) {
			if(group_id <= APP_GROUP_HAS_CATEGORY) {
				lstSkills = skillRepo.findSkillWithCategory(belongto.getId(), group_id, category_id);
			}
			else {
				lstSkills = skillRepo.findSkillWithoutCategory(belongto.getId(), group_id);
			}
		}
		else {
			lstSkills = skillRepo.findSkillWithoutCategory(belongto.getId(), group_id);
		}
		skillsRes.setSkills(lstSkills);
		return ResponseEntity.ok(skillsRes);
	}
	
	public ResponseEntity<?> getSkillLevel(){
		SkillLevelResponse levels = new SkillLevelResponse();
		List<ISkillLevelResponseItem> lstLevels = skillLevelRepo.getAll();
		levels.setLevels(lstLevels);
		return ResponseEntity.ok(levels);
	}
	
	@Transactional
	public ResponseEntity<?> updateEmployeeeSkill(EmployeeSkillEditingRequest employeeSkill, UserPrincipal currentUser){
		boolean update_status= employeeSkillEditingRepository.updateEmployeeSkill(employeeSkill, currentUser);
		return ResponseEntity.ok(new ApiResponse(update_status, "Update employee skill successfully"));
	} 
	
}
